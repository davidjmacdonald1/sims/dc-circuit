package util

import (
	"fmt"
	_ "image/png"
	"math"
	"os"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/davidjmacdonald1/go-generic-vector/vector2"
)

type Sprite struct {
	img  *ebiten.Image
	size vector2.Vector2[int]
}

type SpriteConfig struct {
	name string
	size vector2.Vector2[int]
}

func MakeSpriteConfig(name string, width, height int) SpriteConfig {
	return SpriteConfig{name: name, size: vector2.Make(width, height)}
}

var Sprites map[string]Sprite

func MustLoadSprites(configs []SpriteConfig) {
	Sprites = make(map[string]Sprite, len(configs))
	for _, conf := range configs {
		img, _, err := ebitenutil.NewImageFromFile("res/" + conf.name + ".png")
		if err != nil {
			fmt.Printf("Failed to load sprite %s: %s", conf.name, err)
			os.Exit(1)
		}

		Sprites[conf.name] = Sprite{img: img, size: conf.size}
	}
}

func DrawSprite(
	dst *ebiten.Image,
	name string,
	origin vector2.Vector2[int],
	rotate int,
) {
	rotate %= 4
	sprite := Sprites[name]

	m := ebiten.GeoM{}
	m.Rotate(-math.Pi / 2 * float64(rotate))

	switch rotate {
	case 1:
		origin = origin.AddY(sprite.size.X())
	case 2:
		origin = origin.AddXY(sprite.size.XY())
	case 3:
		origin = origin.AddX(sprite.size.Y())
	}

	m.Translate(vector2.To[float64](origin).XY())
	opts := ebiten.DrawImageOptions{GeoM: m}

	dst.DrawImage(sprite.img, &opts)
}
