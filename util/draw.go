package util

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"gitlab.com/davidjmacdonald1/go-generic-vector/vector2"
)

func StrokeLine[T vector2.Num](
	dst *ebiten.Image,
	v0, v1 vector2.Vector2[T],
	w float32,
	clr color.Color,
) {
	x0, y0 := vector2.To[float32](v0).XY()
	x1, y1 := vector2.To[float32](v1).XY()
	vector.StrokeLine(dst, x0, y0, x1, y1, w, clr, true)
}
