module gitlab.com/davidjmacdonald1/dc-circuit-sim

go 1.21.3

require (
	github.com/hajimehoshi/ebiten/v2 v2.6.4
	gitlab.com/davidjmacdonald1/go-generic-vector v0.0.0-20240128162627-cbe5525a178e
)

require (
	github.com/ebitengine/purego v0.5.0 // indirect
	github.com/jezek/xgb v1.1.0 // indirect
	golang.org/x/exp v0.0.0-20240119083558-1b970713d09a // indirect
	golang.org/x/exp/shiny v0.0.0-20230817173708-d852ddb80c63 // indirect
	golang.org/x/image v0.12.0 // indirect
	golang.org/x/mobile v0.0.0-20230922142353-e2f452493d57 // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
