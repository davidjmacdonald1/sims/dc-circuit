package main

import (
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/davidjmacdonald1/dc-circuit-sim/grid"
	"gitlab.com/davidjmacdonald1/dc-circuit-sim/util"
	"gitlab.com/davidjmacdonald1/go-generic-vector/vector2"
)

type Game struct {
	grid *grid.Grid
}

func (g *Game) Update() error {
	g.grid.Update()
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	g.grid.Draw(screen)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return outsideWidth, outsideHeight
}

func main() {
	size := vector2.Make(1080, 720)
	ebiten.SetWindowSize(size.XY())
	ebiten.SetWindowTitle("DC Circuit Simulation")

	sprites := []util.SpriteConfig{
		util.MakeSpriteConfig("wire", 32, 32),
		util.MakeSpriteConfig("wire_inside", 32, 32),
		util.MakeSpriteConfig("wire_outside", 32, 32),
	}
	util.MustLoadSprites(sprites)

	origin := size.InvScale(2)
	game := &Game{
		grid: grid.NewGrid(origin, 20, 16, 32),
	}

	err := ebiten.RunGame(game)
	if err != nil {
		log.Fatal(err)
	}
}
