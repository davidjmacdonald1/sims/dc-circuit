package grid

import (
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/davidjmacdonald1/dc-circuit-sim/util"
)

type Component interface {
	Draw(dst *ebiten.Image, adj byte)
}

type Wire struct {
	Pos IntPair
}

func (w Wire) Draw(dst *ebiten.Image, adj byte) {
	util.DrawSprite(dst, "wire", w.Pos, 0)
	w.drawConnection(dst, adj, "wire_outside")
	w.drawConnection(dst, adj, "wire_inside")
}

func (w Wire) drawConnection(dst *ebiten.Image, adj byte, img string) {
	for i := byte(1); i <= 8; i *= 2 {
		if adj&i == 0 {
			continue
		}

		rotate := int(math.Log2(float64(i)))
		util.DrawSprite(dst, img, w.Pos, rotate)
	}
}
