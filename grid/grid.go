package grid

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/davidjmacdonald1/dc-circuit-sim/util"
	"gitlab.com/davidjmacdonald1/go-generic-vector/vector2"
)

type IntPair = vector2.Vector2[int]

type Grid struct {
	origin, count IntPair
	cellSize      int
	cells         map[IntPair]Component
}

func NewGrid(center IntPair, countX, countY, cellSize int) *Grid {
	origin := center.SubXY(countX*cellSize/2, countY*cellSize/2)
	count := vector2.Make(countX, countY)

	return &Grid{
		origin:   origin,
		count:    count,
		cellSize: cellSize,
		cells:    make(map[IntPair]Component),
	}
}

func (g *Grid) Update() {
	cursor := g.CursorIndex()
	if cursor == nil {
		return
	}

	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
		g.cells[*cursor] = Wire{Pos: g.IndexPos(*cursor)}
	} else if ebiten.IsMouseButtonPressed(ebiten.MouseButtonRight) {
		delete(g.cells, *cursor)
	}
}

func (g Grid) Draw(dst *ebiten.Image) {
	clr := color.RGBA{R: 150, G: 150, B: 150, A: 150}
	end := g.origin.Add(g.count.Scale(g.cellSize))
	size := end.Sub(g.origin)

	for x := g.origin; x.X() <= end.X(); x = x.AddX(g.cellSize) {
		x2 := x.AddY(size.Y())
		util.StrokeLine(dst, x, x2, 1, clr)
	}

	for y := g.origin; y.Y() <= end.Y(); y = y.AddY(g.cellSize) {
		y2 := y.AddX(size.X())
		util.StrokeLine(dst, y, y2, 1, clr)
	}

	cursor := g.CursorIndex()
	if cursor != nil {
		pos := g.IndexPos(*cursor)
		util.DrawSprite(dst, "wire", pos, 0)
	}

	for p, cell := range g.cells {
		cell.Draw(dst, g.AdjCellFlags(p))
	}
}

func (g Grid) CursorIndex() *IntPair {
	cursor := vector2.Make(ebiten.CursorPosition())
	cursor = cursor.Sub(g.origin)
	if cursor.X() < 0 || cursor.Y() < 0 {
		return nil
	}

	cursor = cursor.InvScale(g.cellSize)
	if cursor.X() >= g.count.X() || cursor.Y() >= g.count.Y() {
		return nil
	}

	cursor = cursor.MulY(-1).AddY(g.count.Y() - 1)
	return &cursor
}

func (g Grid) IndexPos(i IntPair) IntPair {
	i = i.MulY(-1).AddY(g.count.Y() - 1)
	return g.origin.Add(i.Scale(g.cellSize))
}

func (g Grid) AdjCellFlags(p IntPair) (adj byte) {
	_, hasRight := g.cells[p.AddX(1)]
	if p.X() < g.count.X()-1 && hasRight {
		adj ^= 1
	}

	_, hasUp := g.cells[p.AddY(1)]
	if p.Y() < g.count.Y()-1 && hasUp {
		adj ^= 2
	}

	_, hasLeft := g.cells[p.SubX(1)]
	if p.X() > 0 && hasLeft {
		adj ^= 4
	}

	_, hasDown := g.cells[p.SubY(1)]
	if p.Y() > 0 && hasDown {
		adj ^= 8
	}

	return adj
}
